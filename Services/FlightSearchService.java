package org.FlightBooking.Services;

import org.FlightBooking.Model.Flight;
import org.FlightBooking.Model.FlightRoutes;
import org.FlightBooking.Model.FlightSeatsAllotment;
import org.FlightBooking.Repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;



@Service
public class FlightSearchService
{

    @Autowired
    FlightRepository flightRepository;
/*
    public List<FlightRoutes> searchByDateOnFlight( String source,  String destination,String departure) {

        flightRepository = new FlightRepository();
        List<Flight> flightRepo = flightRepository.addFlight();
        List<FlightRoutes> flightRoute = flightRepository.addRoutes();

        List<FlightRoutes> resultFlight = new ArrayList<FlightRoutes>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();
       // System.out.println(dtf.format(localDate)); //2016/11/16

        if (flightRoute.size() == 0) {
            return new ArrayList<>();
        } else {
            for (FlightRoutes froute : flightRoute) {
                if ((froute.getfDeparatureCity().equals(source)) && (froute.getfArrivalCity().equals(destination))) {

                    String departureDate = froute.getfDepartureDate();
                    System.out.println("date  is : " + departureDate);

                    if (departureDate == null) {
                        departureDate = localDate.toString();
                        System.out.println("date  is : " + departureDate);

                    }

                    if (departureDate == departure)
                        resultFlight.add(froute);
                }
            }
        }

        return resultFlight;
    }
    }
*/

    public List<Flight> searchSeatOnFlight( String source,
                                            String destination,
                                            Integer passengerCount,
                                            String departureDate,
                                            String flightClass)
    {

        flightRepository = new FlightRepository();

        List<Flight> flightRepo = flightRepository.addFlight();

        List <Flight> resultFlight = new ArrayList<Flight>();


        if(flightRepo.size()==0)
        {
            return new ArrayList<>();
        }
        else
            {
                System.out.println("date  is : " + departureDate +flightClass+" S "+source+"D " +destination +"C "+passengerCount);

                if (passengerCount == null)
                    passengerCount = 1;


                if(departureDate.isEmpty())
                {
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDate localeDate = LocalDate.now();
                    departureDate = String.valueOf(localeDate.plusDays(1));
                    System.out.println("date after today is : " + departureDate);
                }

                for (Flight f1 : flightRepo)
                {
                        if ((f1.getFlightSource().equals(source)) && (f1.getFlightDestination().equals(destination))
                        && (f1.getFlightDepartureDate().equals(departureDate)))
                        {
                            String flightName=f1.getFlightName();
                            System.out.println("flightname is " +flightName);

                            if(flightClass.isEmpty())
                                flightClass="EC";

                           int seatingAvailable= getFlightClassSeats(flightName,flightClass);

                          //  int seatingAvailable = f1.getFlightCapacity();
                            System.out.println("seating is : " + seatingAvailable);

                            if (seatingAvailable >= passengerCount)
                                resultFlight.add(f1);


                          }
                }
             }
        return resultFlight;

    }

    private int getFlightClassSeats(String flightName,String flightClass)
    {
       // System.out.println("inside:");
       // System.out.println("flight class is :"+flightClass +"  "+flightName);
        List<FlightSeatsAllotment> flightSeats = flightRepository.addSeatAllotment();
        int availableSeats=0;
        for (FlightSeatsAllotment f2 : flightSeats)
          if((f2.getFlightName().equals(flightName) && (f2.getFlightClass().equals(flightClass))))
             {
                 availableSeats = f2.getSeatsAlloted();
                // System.out.println("seats alloted are :" +availableSeats);
              }
         return availableSeats;
    }


}

