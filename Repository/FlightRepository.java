package org.FlightBooking.Repository;

import org.FlightBooking.Model.Flight;
import org.FlightBooking.Model.FlightRoutes;
import org.FlightBooking.Model.FlightSeatsAllotment;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Repository
public class FlightRepository {


    public FlightRepository()
          {}


    public ArrayList<Flight> addFlight() {

        ArrayList<Flight> fm = new ArrayList<Flight>();

        Flight fm1 = new Flight("Boeing777", "Hyderabad", "Delhi", 238, 6000,"2019-08-19");
        Flight fm2 = new Flight("Airbus319V2", "Delhi", "Hyderabad", 144, 4000,"2019-09-20");
        Flight fm3 = new Flight("Airbus321", "Bombay", "Delhi", 172, 5000,"2019-08-28");
        fm.add(fm1);
        fm.add(fm2);
        fm.add(fm3);
        Flight fm4 = new Flight("Airbus321", "Delhi", "Hyderabad", 172, 5000,"2019-08-24");
        Flight fm5 = new Flight("AirBus319V2", "Delhi", "Bombay", 144, 4000,"2019-08-25");
        Flight fm6 = new Flight("Boeing777", "Bombay", "Delhi", 238, 6000, "2019-08-21");
        fm.add(fm4);
        fm.add(fm5);
        fm.add(fm6);
        Flight fm7 = new Flight("Airbus319V2", "Banglore", "Hyderabad", 144, 4000,"2019-08-21");
        Flight fm8 = new Flight("Boeing777", "Banglore", "Bombay", 238, 6000,"2019-08-22");
        Flight fm9 = new Flight("Airbus321", "Hyderabad", "Delhi", 172, 5000,"2019-08-23");
        fm.add(fm7);
        fm.add(fm8);
        fm.add(fm9);
        return fm;
    }
    //boeing 8+35+195 6000
    //v2 0+0+144 4000
    //321 0+20+152 5000

public ArrayList<FlightSeatsAllotment> addSeatAllotment()
{
    ArrayList<FlightSeatsAllotment> flightSeatsAllotments = new ArrayList<FlightSeatsAllotment>();
    FlightSeatsAllotment fSA1 = new FlightSeatsAllotment(1,"Boeing777","BC",35);
    FlightSeatsAllotment fSA2 = new FlightSeatsAllotment(1,"Boeing777","FC",8);
    FlightSeatsAllotment fSA3 = new FlightSeatsAllotment(1,"Boeing777","EC",195);
    FlightSeatsAllotment fSA4 = new FlightSeatsAllotment(2,"Airbus319V2","BC",0);
    FlightSeatsAllotment fSA5 = new FlightSeatsAllotment(2,"Airbus319V2","FC",0);
    FlightSeatsAllotment fSA6 = new FlightSeatsAllotment(2,"Airbus319V2","EC",144);
    FlightSeatsAllotment fSA7 = new FlightSeatsAllotment(3,"Airbus321","EC",152);
    FlightSeatsAllotment fSA8 = new FlightSeatsAllotment(3,"Airbus321","BC",20);
    FlightSeatsAllotment fSA9 = new FlightSeatsAllotment(3,"Airbus321","FC",0);
    flightSeatsAllotments.add(fSA1);
    flightSeatsAllotments.add(fSA2);
    flightSeatsAllotments.add(fSA3);
    flightSeatsAllotments.add(fSA4);
    flightSeatsAllotments.add(fSA5);
    flightSeatsAllotments.add(fSA6);
    flightSeatsAllotments.add(fSA7);
    flightSeatsAllotments.add(fSA8);
    flightSeatsAllotments.add(fSA9);

    return flightSeatsAllotments;
}




     public ArrayList<FlightRoutes> addRoutes() {

//"yyyy-mm-dd"

         ArrayList<FlightRoutes> fr = new ArrayList<FlightRoutes>();
         FlightRoutes fr1 = new FlightRoutes(1, "Hyderabad", "Delhi", "2019-08-29");
         fr.add(fr1);
         FlightRoutes fr2 = new FlightRoutes(1, "Hyderabad", "Delhi", "2019-08-28");
         fr.add(fr2);
         FlightRoutes fr3 = new FlightRoutes(1, "Hyderabad", "Delhi", "2019-08-19");
         fr.add(fr1);
         FlightRoutes fr4 = new FlightRoutes(4, "Banglore", "Delhi", "2019-08-29");
         fr.add(fr4);
         FlightRoutes fr5 = new FlightRoutes(4, "Banglore", "Delhi", "2019-08-28");
         fr.add(fr5);
         FlightRoutes fr6 = new FlightRoutes(4, "Banglore", "Delhi", "2019-09-11");
         fr.add(fr6);
         FlightRoutes fr7 = new FlightRoutes(2, "Hyderabad", "Banglore", "2019-08-20");
         fr.add(fr7);

         FlightRoutes fr8 = new FlightRoutes(3, "Delhi", "Hyderabad", "2019-08-20");
         fr.add(fr8);
         FlightRoutes fr9 = new FlightRoutes(5, "Delhi", "Banglore", "2019-08-20");
         fr.add(fr9);




return fr;
     }


    }



