package org.FlightBooking.Controller;
import org.FlightBooking.Model.AjaxResponseBody;
import org.FlightBooking.Model.FlightRoutes;
import org.FlightBooking.Model.FlightSeatsAllotment;
import org.FlightBooking.Services.FlightSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.FlightBooking.Model.Flight;
import java.util.stream.Collectors;

@RestController
public class FlightController {

    @Autowired
    FlightSearchService flightSearchService;

    //  @RequestMapping(path = "/searchSeatOnFlight", method = RequestMethod.GET)
    //  public List<Flight> searchSeatOnFlight(@RequestParam String source, @RequestParam String destination, @RequestParam
    //        Integer passengerCount) {
    // return flightSearchService.searchSeatOnFlight(source,destination,passengerCount);
    //  }

    @PostMapping("/searchSeatOnFlight")
    public ResponseEntity<?> searchResultViaAjax(@RequestBody FlightSeatsAllotment searchFlight, Errors errors) {

        AjaxResponseBody ajaxresponse = new AjaxResponseBody();
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            ajaxresponse.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            ResponseEntity.badRequest().body(ajaxresponse);
        }
        ajaxresponse.result = flightSearchService.searchSeatOnFlight(searchFlight.getFlightSource(),
                searchFlight.getFlightDestination(),
                searchFlight.getFlightCapacity(),
                searchFlight.getFlightDepartureDate(),
                searchFlight.getFlightClass()
        );
       // System.out.println(ajaxresponse.result);
        ajaxresponse.setResult(ajaxresponse.result);
        return ResponseEntity.ok(ajaxresponse);
    }
}
/*
    @PostMapping("/searchDepartureDateOnFlight")
    public ResponseEntity<?> searchResultViaAjax(@RequestBody FlightRoutes searchFlight, Errors errors) {

        AjaxResponseBody ajaxresponse = new AjaxResponseBody();
        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            ajaxresponse.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            ResponseEntity.badRequest().body(ajaxresponse);
        }
        // ajaxresponse.result = flightSearchService.searchSeatOnFlight(searchFlight.getFlightSource(), searchFlight.getFlightDestination(), searchFlight.getFlightCapacity());
        ajaxresponse.dateResult = flightSearchService.searchByDateOnFlight(searchFlight.getfDeparatureCity(),
                searchFlight.getfArrivalCity(), searchFlight.getfDepartureDate());


        ajaxresponse.setDateResult(ajaxresponse.dateResult);
        return ResponseEntity.ok(ajaxresponse);
    }
}

*/


