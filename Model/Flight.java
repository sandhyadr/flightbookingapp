package org.FlightBooking.Model;

import java.util.Date;
import java.util.Objects;

public class Flight {

    String flightName;
    String flightSource;
    String flightDestination;
    int    flightCapacity;
    double flightBasePrice;
    String flightDepartureDate;


    public String getFlightDepartureDate() {
        return flightDepartureDate;
    }

    public void setFlightDepartureDate(String flightDepartureDate) {
        this.flightDepartureDate = flightDepartureDate;
    }


    public Flight(String flightName, String flightSource, String flightDestination,
                  int flightCapacity, double flightBasePrice, String flightDepartureDate)
    {
        this.flightName = flightName;
        this.flightSource = flightSource;
        this.flightDestination = flightDestination;
        this.flightCapacity = flightCapacity;
        this.flightBasePrice = flightBasePrice;
        this.flightDepartureDate = flightDepartureDate;
    }

    public Flight()
    { }

    public String getFlightSource() {
        return flightSource;
    }

    public void setFlightSource(String flightSource) {
        this.flightSource = flightSource;
    }

    public String getFlightDestination() {
        return flightDestination;
    }

    public void setFlightDestination(String flightDestination) {
        this.flightDestination = flightDestination;
    }

    public int getFlightCapacity() {
        return flightCapacity;
    }

    public void setFlightCapacity(int flightCapacity) {
        this.flightCapacity = flightCapacity;
    }

    public double getFlightBasePrice() {
        return flightBasePrice;
    }

    public void setFlightBasePrice(double flightBasePrice) {
        this.flightBasePrice = flightBasePrice;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flight flight = (Flight) o;
        return flightCapacity == flight.flightCapacity &&
                Double.compare(flight.flightBasePrice, flightBasePrice) == 0 &&

                Objects.equals(flightName, flight.flightName) &&
                flightSource.equals(flight.flightSource) &&
                flightDestination.equals(flight.flightDestination) &&
                Objects.equals(flightDepartureDate, flight.flightDepartureDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash( flightName, flightSource, flightDestination, flightCapacity, flightBasePrice, flightDepartureDate);
    }
}
