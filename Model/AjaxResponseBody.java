package org.FlightBooking.Model;

import org.FlightBooking.Model.Flight;

import java.util.List;

public class AjaxResponseBody {

    public String msg;
    public List<Flight> result;
    public List<FlightRoutes> dateResult;

    public List<FlightRoutes> getDateResult() {
        return dateResult;
    }

    public void setDateResult(List<FlightRoutes> dateResult) {
        this.dateResult = dateResult;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<Flight> getResult() {
        return result;
    }

    public void setResult(List<Flight> result) {
        this.result = result;
    }

}