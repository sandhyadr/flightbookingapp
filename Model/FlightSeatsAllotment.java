package org.FlightBooking.Model;

public class FlightSeatsAllotment extends Flight{
    int    flightId;
    String flightName;
    String flightClass;
    int    seatsAlloted;

    public FlightSeatsAllotment() {
    }

    public FlightSeatsAllotment(int flightId, String flightName, String flightClass, int seatsAlloted) {
        this.flightId = flightId;
        this.flightName = flightName;
        this.flightClass = flightClass;
        this.seatsAlloted = seatsAlloted;
    }




    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightClass() {
        return flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public int getSeatsAlloted() {
        return seatsAlloted;
    }

    public void setSeatsAlloted(int seatsAlloted) {
        this.seatsAlloted = seatsAlloted;
    }
}
