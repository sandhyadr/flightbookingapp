package org.FlightBooking.Model;

import org.FlightBooking.Model.Flight;

import java.util.Date;
import java.util.Objects;

public class FlightRoutes  {

    int    routeId;
    String fDeparatureCity;
    String fArrivalCity;
    String fDepartureDate;

    public String getfDepartureDate() {
        return fDepartureDate;
    }

    public void setfDepartureDate(String fDepartureDate) {
        this.fDepartureDate = fDepartureDate;
    }

    public FlightRoutes(int routeId, String fDeparatureCity, String fArrivalCity, String fDepartureDate) {
        this.routeId = routeId;
        this.fDeparatureCity = fDeparatureCity;
        this.fArrivalCity = fArrivalCity;
        this.fDepartureDate = fDepartureDate;
    }





    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getfDeparatureCity() {
        return fDeparatureCity;
    }

    public void setfDeparatureCity(String fDeparatureCity) {
        this.fDeparatureCity = fDeparatureCity;
    }


    public String getfArrivalCity() {
        return fArrivalCity;
    }

    public void setfArrivalCity(String fArrivalCity) {
        this.fArrivalCity = fArrivalCity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightRoutes that = (FlightRoutes) o;
        return routeId == that.routeId &&
                fDeparatureCity.equals(that.fDeparatureCity) &&
                fArrivalCity.equals(that.fArrivalCity) &&
                Objects.equals(fDepartureDate, that.fDepartureDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(routeId, fDeparatureCity, fArrivalCity, fDepartureDate);
    }
}
