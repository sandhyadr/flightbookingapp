package org.FlightBooking.Model;

public class FlightFares {

    int    routeId;
    double firstClassBaseFare;
    double businessClassBaseFare;
    double economyClassBaseFare;
    double baseFare;

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public double getFirstClassBaseFare() {
        return firstClassBaseFare;
    }

    public void setFirstClassBaseFare(double firstClassBaseFare) {
        this.firstClassBaseFare = firstClassBaseFare;
    }

    public double getBusinessClassBaseFare() {
        return businessClassBaseFare;
    }

    public void setBusinessClassBaseFare(double businessClassBaseFare) {
        this.businessClassBaseFare = businessClassBaseFare;
    }

    public double getEconomyClassBaseFare() {
        return economyClassBaseFare;
    }

    public void setEconomyClassBaseFare(double economyClassBaseFare) {
        this.economyClassBaseFare = economyClassBaseFare;
    }

    public double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(double baseFare) {
        this.baseFare = baseFare;
    }
}
